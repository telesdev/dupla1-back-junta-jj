const express = require('express')
var { Usuario, listaUsuarios } = require('../../models/usuario')
const router = express.Router()

router.get('/', (req, res, next) => {
    try{
        res.send(listaUsuarios)
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.get('/:userId', (req, res, next)=> {
    try{
      let usuario =  listaUsuarios.filter(u => u.id == req.params["userId"])
      if (usuario.length > 0){
        res.send(usuario[0])
      }else{
        res.status(404).send({"erro" : "Usuário não existe"})
      }
    }catch(err){
      console.error(err.message)
      res.status(500).send({"error" : "Server Error"})
    }
})

router.post('/', (req, res, next) => {
    try{
        let {email, nome, cidade, bairro, telefone, isAluno, isProfessor} = req.body

        if (!email){
            res.status(400).send({"erro" : "Cadê o email?"})
        } else if (!nome){
            res.status(400).send({"erro" : "Cadê o nome?"})
        } else if (!cidade){
            res.status(400).send({"erro" : "Cadê a cidade?"})
        } else if (!bairro){
            res.status(400).send({"erro" : "Cadê o bairro?"})
        } else if (!telefone){
            res.status(400).send({"erro" : "Cadê o telefone?"})
        } else if (isAluno == undefined){
            res.status(400).send({"erro" : "É ou não é aluno?"})
        } else if (isProfessor == undefined){
            res.status(400).send({"erro" : "É ou não é professor?"})
        } else  {
            let usuario = new Usuario(email=email, nome=nome, cidade=cidade, bairro=bairro, telefone=telefone, isAluno=isAluno, isProfessor=isProfessor)

            listaUsuarios.push(usuario)
            res.send(listaUsuarios)
        }
    } catch(err) {
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.put('/:userId' , (req, res) => {
    try {
        let usuario = listaUsuarios.filter(u => u.id == req.params["userId"])
        if (usuario.length > 0) {
            usuario = usuario[0]
            let {email, nome, cidade, bairro, telefone, isAluno, isProfessor} = req.body

            if (!email){
                res.status(400).send({"erro" : "Cadê o email?"})
            } else if (!nome){
                res.status(400).send({"erro" : "Cadê o nome?"})
            } else if (!cidade){
                res.status(400).send({"erro" : "Cadê a cidade?"})
            } else if (!bairro){
                res.status(400).send({"erro" : "Cadê o bairro?"})
            } else if (!telefone){
                res.status(400).send({"erro" : "Cadê o telefone?"})
            } else if (isAluno == undefined){
                res.status(400).send({"erro" : "É ou não é aluno?"})
            } else if (isProfessor == undefined){
                res.status(400).send({"erro" : "É ou não é professor?"})
            } else  {
                for (const [chave, valor] of Object.entries(req.body)) {
                    usuario[chave] = valor
                }
                res.send(usuario)
            }
        } else {
            res.status(404).send({"err" : "Usuário não existe, pooo"})
        }
    } catch(err) {
        res.status(500).send({"err" : "Server Error"})
    }
})

router.patch('/:userId', (req, res, next)=> {
    try{
        let usuario =  listaUsuarios.filter(u => u.id == req.params["userId"])
        if (usuario.length > 0){
            usuario = usuario[0]
            for (const [chave, valor] of Object.entries(req.body)) {
                if (valor == undefined || valor == null || valor == ""){
                    continue
                }
                usuario[chave] = valor
            }
            res.send(usuario)
        } else {
            res.status(404).send({"erro" : "Usuário não existe"})
        }
    } catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.delete('/:userId', (req, res, next)=> {
    try{
      let usuario = listaUsuarios.filter(u => u.id == req.params["userId"])
      if (usuario.length > 0){
        listaUsuarios =  listaUsuarios.filter(u => u.id != req.params["userId"])
        res.send(listaUsuarios)
      }else{
        res.status(404).send({"erro" : "Usuário não existe"})
      }
    }catch(err){
      console.error(err.message)
      res.status(500).send({"error" : "Server Error"})
    }
})


module.exports = router