class Usuario {
    constructor(email, nome, cidade, bairro, telefone, isAluno, isProfessor) {
        this.id = this.gerarId()
        this.email = email
        this.nome = nome
        this.cidade = cidade
        this.bairro = bairro
        this.telefone = telefone
        this.isAluno = isAluno
        this.isProfessor = isProfessor
    }

    gerarId() {
        if (listaUsuarios.length === 0) {
            return 1
        }
        return listaUsuarios[listaUsuarios.length-1].id + 1
    }
}

const listaUsuarios = []

listaUsuarios.push(new Usuario("teste@teste.com", "Usuário 1", "Rio de Janeiro", "Recreio dos Bandeirantes", 21999999999, true, false))

listaUsuarios.push(new Usuario("test2e@teste2.com", "Usuário 2", "Rio de Janeiro", "Barra da Tijuca", 21999999998, false, true))

listaUsuarios.push(new Usuario("test3e@teste3.com", "Usuário 3", "Niterói", "Icaraí", 2199999997, true, true))


module.exports = { Usuario, listaUsuarios }